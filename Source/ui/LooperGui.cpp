//
//  LooperGui.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "LooperGui.h"

LooperGui::LooperGui(Looper& looper_) : looper (looper_)
{
    playButton.setButtonText ("Play");
    playButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    playButton.setColour(TextButton::buttonColourId, Colours::grey);
    playButton.setColour(TextButton::buttonOnColourId, Colours::lightgrey);
    addAndMakeVisible (&playButton);
    playButton.addListener (this);
    
    recordButton.setButtonText ("Record");
    recordButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    recordButton.setColour(TextButton::buttonColourId, Colours::darkred);
    recordButton.setColour(TextButton::buttonOnColourId, Colours::red);
    addAndMakeVisible (&recordButton);
    recordButton.addListener (this);
    
    deleteButton.setButtonText("Delete");
    deleteButton.setConnectedEdges(Button::ConnectedOnLeft | Button::ConnectedOnRight);
    deleteButton.setColour(TextButton::buttonColourId, Colours::blue);
    addAndMakeVisible(deleteButton);
    deleteButton.addListener(this);
    
    waveformComboBox.addItem("Sine", 1);
    waveformComboBox.addItem("Square", 2);
    waveformComboBox.addItem("Saw", 3);
    addAndMakeVisible(waveformComboBox);
    waveformComboBox.setSelectedId(1);
    waveformComboBox.addListener(this);
    
    gainSlider.setSliderStyle(juce::Slider::LinearHorizontal);
    gainSlider.setRange(0.f, 1.f);
    gainSlider.setValue(1.f);
    gainSlider.addListener(this);
    addAndMakeVisible(gainSlider);
    
    String gainText = "Gain";
    gainLabel.setText(gainText, dontSendNotification);
    addAndMakeVisible(gainLabel);
}

void LooperGui::buttonClicked (Button* button)
{
    if (button == &playButton)
    {
        looper.setPlayState (!looper.getPlayState());
        playButton.setToggleState (looper.getPlayState(), dontSendNotification);
        if (looper.getPlayState())
        {
            playButton.setButtonText ("Stop");
        }
        else
        {
            playButton.setButtonText ("Play");
            if (looper.getRecordState() == true)
            {
                looper.setRecordState (!looper.getRecordState());
                recordButton.setToggleState (looper.getRecordState(), dontSendNotification);
            }
        }
    }
    else if (button == &recordButton)
    {
        looper.setRecordState (!looper.getRecordState());
        recordButton.setToggleState (looper.getRecordState(), dontSendNotification);

    }
    else if (button == &deleteButton)
    {
        if (looper.getPlayState() == false)
        {
            looper.deleteLoop();
        }
        else
        {
            AlertWindow::showMessageBox (AlertWindow::WarningIcon,
                                         "Warning!",
                                         "Please Stop Playing Before Deleting!\n");
        }
        
    }
}

void LooperGui::comboBoxChanged(ComboBox* comboBox)
{
    looper.setWaveform(waveformComboBox.getSelectedId());
}


void LooperGui::sliderValueChanged (Slider* slider)
{
    looper.setOverallGain(gainSlider.getValue());
}

void LooperGui::resized()
{
    playButton.setBounds (0, 0, getWidth()/4, getHeight()/2);
    recordButton.setBounds(getWidth()/4, 0, getWidth()/4, getHeight()/2);
    deleteButton.setBounds(2*getWidth()/4, 0, getWidth()/4, getHeight()/2);
    waveformComboBox.setBounds(3*getWidth()/4, 0, getWidth()/4, getHeight()/2);
    gainSlider.setBounds(50, getHeight()/2, getWidth()-50, getHeight()/2);
    gainLabel.setBounds(0, getHeight()/2, 50, getHeight()/2);
    
    //recordButton.setBounds (playButton.getBounds().translated(getWidth()/2, 0));
}