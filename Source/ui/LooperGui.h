//
//  LooperGui.h
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#ifndef H_LooperGui
#define H_LooperGui

#include "../JuceLibraryCode/JuceHeader.h"
#include "Looper.h"

/**
 Gui for the looper class
 */
class LooperGui :   public Component,
                    public Button::Listener,
                    public ComboBox::Listener,
                    public Slider::Listener
{
public:
    /**
     constructor - receives a reference to a Looper object to control
     */
    LooperGui(Looper& looper_);
    
    //Button Listener
    void buttonClicked (Button* button) override;
    
    //ComboBox Listener
    void comboBoxChanged (ComboBox* comboBox) override;
    
    //Slider Listener
    void sliderValueChanged (Slider* slider) override;
    
    //Component
    void resized() override;
private:
    Looper& looper;                 //reference to a looper object
    TextButton playButton;
    TextButton recordButton;
    TextButton deleteButton;
    ComboBox waveformComboBox;
    Slider gainSlider;
    Label gainLabel;
};

#endif
