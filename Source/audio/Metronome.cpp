//
//  Metronome.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 24/11/2016.
//
//

#include "Metronome.hpp"

Metronome::Metronome()
{
    //position to the start of audioSampleBuffer
    bufferPosition = 0;
    
    metronomeState = false;
}

Metronome::~Metronome()
{
    
}

void Metronome::incrementMetronome()
{
    //increment and cycle the buffer counter
    ++bufferPosition;
    if (bufferPosition == currentBufferSize)
    {
        bufferPosition = 0;
    }
}

bool Metronome::getMetronome()
{
    if ((bufferPosition % (currentBufferSize / 5)) == 0 && metronomeState == 1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

int Metronome::getBufferSize()
{
    return bufferSize;
}

int Metronome::getBufferPosition()
{
    return bufferPosition;
}
void Metronome::setMetronomeState (const bool newState)
{
    metronomeState = newState;
}

bool Metronome::getMetronomeState () const
{
    return metronomeState;
}

void Metronome::setBpm(float newBpm)
{
    bpm = newBpm;
    currentBufferSize = (60 * sampleRate * 5)/bpm;
}