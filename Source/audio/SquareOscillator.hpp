//
//  SquareOscillator.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 01/12/2016.
//
//

#ifndef SquareOscillator_hpp
#define SquareOscillator_hpp

#include <stdio.h>
#include "Oscillator.hpp"

/**
 Class for a square wave oscillator.
 @see Oscillator class for base class info
 @see Oscillator
 */

class SquareOscillator  : public Oscillator

{
public:
    /**Oscillator constructor*/
    SquareOscillator();
    
    /**Oscillator destructor*/
    virtual ~SquareOscillator();
    
    /** function that provides the execution of the waveshape
     @param float currentPhase is the phase position of the waveform and it's usually between 0 and 2*M_PI
     @return returns a float amplitude value of the waveform at the current phase position. As this is a quare wave, it will be either -1 or +1 */
    float renderWaveShape(const float currentPhase) override;
    
    /** function that sets the duty cycle of the square wave
     @param float newDutyCycle is the Duty Cycle value in percentage between 0% and 100% */
    void setDutyCycle(float newDutyCycle);
    
private:
    float dutyCycle;
    
};

#endif /* SquareOscillator_hpp */
