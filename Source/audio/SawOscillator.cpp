//
//  SawOscillator.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 01/12/2016.
//
//

#include "SawOscillator.hpp"
#include <cmath>

SawtoothOscillator::SawtoothOscillator()
{
    //setDutyCycle (50);
}
SawtoothOscillator::~SawtoothOscillator()
{
    
}

float SawtoothOscillator::renderWaveShape(const float currentPhase)
{
    if (currentPhase <= M_PI)
    {
        return currentPhase / M_PI;
    }
    else
    {
        return (currentPhase/M_PI)-2;
    }
}