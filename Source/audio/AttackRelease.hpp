//
//  AttackRelease.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 01/12/2016.
//
//

#ifndef AttackRelease_hpp
#define AttackRelease_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

/**
 Attack Release Class to get rid of the clicks
 */
class AttackRelease
{
public:
    /**
     Constructor - initialise everything
     */
    AttackRelease ();
    
    /**
     Destructor
     */
    ~AttackRelease();
    
    /**
     Returns the ramp value for the attack
     */
    float attack ();
    
    /**
     Returns the ramp value for the release
     */
    float release ();
    
    void setSampleRate (int newSampleRate) { sampleRate = newSampleRate; }
    void resetAttackCounter () { attackCounter = 0.f; }
    void resetReleaseCounter () { releaseCounter = 1.f; }
    
private:
    const float attackTime = 0.01;
    const float releaseTime = 0.1;
    
    float attackCounter;
    float releaseCounter;
    float sampleRate;
};
#endif /* AttackRelease_hpp */
