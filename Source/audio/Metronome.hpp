//
//  Metronome.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 24/11/2016.
//
//

#ifndef Metronome_hpp
#define Metronome_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class Metronome
{
public:
    /**
     Constructor - initialise everything
     */
    Metronome();
    
    /**
     Destructor
     */
    ~Metronome();
    
    void incrementMetronome();
    bool getMetronome();
    int getBufferSize();
    int getBufferPosition();
    /**
     Sets/unsets the metronome state
     */
    void setMetronomeState (bool newState);
    
    /**
     Gets the the metronome state
     */
    bool getMetronomeState() const;
    
    void setBpm(float newBpm);
    void setSampleRate(int newSampleRate) {sampleRate = newSampleRate;}
    
private:
    static const int bufferSize = 44100*3*5; //Because the longest buffer size needed is when at 20BPM.
    int currentBufferSize;
    unsigned int bufferPosition;
    int sampleRate;
    
    int metronomeState;
    float bpm;
};

#endif /* Metronome_hpp */
