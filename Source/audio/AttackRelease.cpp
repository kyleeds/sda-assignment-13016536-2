//
//  AttackRelease.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 01/12/2016.
//
//

#include "AttackRelease.hpp"

AttackRelease::AttackRelease()
{
    attackCounter = 0.f;
    releaseCounter = 1.f;
}

AttackRelease::~AttackRelease()
{
    
}

float AttackRelease::attack()
{
    if (attackCounter < sampleRate*attackTime) {
        float attackGain = attackCounter/(sampleRate*attackTime);
        //DBG("Attack Value: " << attackGain << "\n");
        attackCounter++;
        return attackGain;
    }
    else
        return 1.f;
}

float AttackRelease::release()
{
    if (releaseCounter < sampleRate*releaseTime) {
        float releaseGain = 1.f - (releaseCounter/(sampleRate*releaseTime));
        //DBG("Release Value: " << releaseGain << "\n");
        releaseCounter++;
        return releaseGain;
    }
    else
    {
        if (releaseCounter == sampleRate*releaseTime) {
            releaseCounter++;
            return 0.f;
        }
        else
        {
            return 1.f;
        }
    }
}