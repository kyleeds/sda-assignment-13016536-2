//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 29/11/2016.
//
//

#include "SinOscillator.hpp"
#include <cmath>

SinOscillator::SinOscillator()
{
    reset();
}

SinOscillator::~SinOscillator()
{
    
}

float SinOscillator::renderWaveShape (const float currentPhase)
{
    return sin (currentPhase);
}