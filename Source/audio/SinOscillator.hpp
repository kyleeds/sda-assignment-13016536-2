//
//  SinOscillator.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 29/11/2016.
//
//

#ifndef SinOscillator_hpp
#define SinOscillator_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "Oscillator.hpp"

/**
 Class for a sine oscillator.
 @see Oscillator
 */

class SinOscillator  : public Oscillator
{
public:
    /**SinOscillator constructor*/
    SinOscillator();
    
    /**SinOscillator destructor*/
    virtual ~SinOscillator();
    
    /** function that provides the execution of the waveshape
     @param float currentPhase is the phase position of the waveform and it's usually between 0 and 2*M_PI
     @return returns a float amplitude value of the waveform at the current phase position. As this is a sine wave, it will be between -1 and +1 */
    virtual float renderWaveShape (const float currentPhase) override;
    
private:
    
};

#endif /* SinOscillator_hpp */
