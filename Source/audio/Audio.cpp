/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    File audioFile("/Volumes/DataDrive/Users/k35-edwards/Desktop/Kyle/Met_Low.wav");
    if (audioFile.existsAsFile()) {
        metronomeFilePlayer.loadFile(File ("/Volumes/DataDrive/Users/k35-edwards/Desktop/Kyle/Met_Low.wav"));
        DBG("File path does exist\n");
    }
    else { DBG("File path doesn't exist\n"); }
    
    loopers.add (new Looper (metronome));
    loopers.add (new Looper (metronome));
    
    //load the filePlayer into the audio source
    audioSourcePlayer.setSource (&metronomeFilePlayer);
    
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}


void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message_)
{
    //All MIDI inputs arrive here
    if (message_.isNoteOn())
    {
        for (int counter = 0; counter < 2; counter++) {
            loopers[counter]->startAttack();
            loopers[counter]->setFrequency(MidiMessage::getMidiNoteInHertz (message_.getNoteNumber()));
            loopers[counter]->setAmplitude(message_.getFloatVelocity());
        }
    }
    else if (message_.isNoteOff())
    {
        for (int counter = 0; counter < 2; counter++) {
            loopers[counter]->startRelease();
        }
    }
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    // get the audio from our file player - player puts samples in the output buffer
    audioSourcePlayer.audioDeviceIOCallback (inputChannelData, numInputChannels, outputChannelData, numOutputChannels, numSamples);
    
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while(numSamples--)
    {
        float output = 0.f;
        
        //increment and cycle the buffer counter if the playState is true
        if (playState == true) {
            metronome.incrementMetronome();
        }
        
        //Playing the metronome clicks whenever the true message comes in
        if (loopers[0]->getMetronomeClick() == true)
        {
            metronomeFilePlayer.setPlaying(!metronomeFilePlayer.isPlaying());
            if (metronomeFilePlayer.isPlaying() == true)
            {
                //metronomeTimer.startTimer(250);
            }
            else
            {
                //metronomeTimer.stopTimer();
            }
        }
        
        //Playing the loopers that have the playState set as true
        if (loopers[0]->getPlayState() == true && loopers[1]->getPlayState() == false)
        {
            output = loopers[0]->processSample (*inL);
        }
        else if (loopers[1]->getPlayState() == true && loopers[0]->getPlayState() == false)
        {
            output = loopers[1]->processSample (*inL);
        }
        else if (loopers[0]->getPlayState() == true && loopers[1]->getPlayState() == true)
        {
            output = loopers[0]->processSample (*inL) + loopers[1]->processSample(*inL);
        }
        
        if (playState == true && inputState == false)
        {
            *outL = output+*inL;
            *outR = output+*inL;
        }
        else {
            *outL = output;
            *outR = output;
        }

        inL++;
        inR++;
        outL++;
        outR++;
    }
}
void Audio::setPlayState(bool newPlayState)
{
    playState = newPlayState;
    for (int counter = 0; counter < 2; counter++) {
        
        //So when you click 'Play' on individual track, it doesn't play the release of the last note played
        loopers[counter]->setFrequency(0.f);
        loopers[counter]->setAmplitude(0.f);
    }
    
}

void Audio::setInputState(bool newInputState)
{
    inputState = newInputState;
    loopers[0]->setInputState(newInputState);
    loopers[1]->setInputState(newInputState);
}
bool Audio::getInputState()
{
    return inputState;
}

void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    loopers[0]->setSampleRate(device->getCurrentSampleRate());
    loopers[1]->setSampleRate(device->getCurrentSampleRate());
    
    audioSourcePlayer.audioDeviceAboutToStart (device);
}

void Audio::audioDeviceStopped()
{
    audioSourcePlayer.audioDeviceStopped();
}