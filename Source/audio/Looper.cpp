//
//  Looper.cpp
//  sdaLooper
//
//  Created by tj3-mitchell on 21/01/2013.
//
//

#include "Looper.h"

Looper::Looper (Metronome& metronome_) : metronome (metronome_)
{
    voices.add (new Voices);
    voices.add (new Voices);
    voices.add (new Voices);
    
    //initialise - not playing / recording
    playState = false;
    recordState = false;
    
    //audioSampleBuffer contents to zero
    audioSampleBuffer.setSize (1, metronome.getBufferSize());
    audioSampleBuffer.clear();
}

Looper::~Looper()
{
    
}

void Looper::setPlayState (const bool newState)
{
    playState = newState;
}

bool Looper::getPlayState () const
{
    return playState.get();
}

void Looper::setRecordState (const bool newState)
{
    recordState = newState;
}

bool Looper::getRecordState () const
{
    return recordState.get();
}

void Looper::setInputState (const bool newState)
{
    inputState = newState;
}

bool Looper::getInputState () const
{
    return inputState.get();
}
void Looper::setWaveform(int newWaveformId)
{
    voices[0]->setWaveform(newWaveformId);
    voices[1]->setWaveform(newWaveformId);
    voices[2]->setWaveform(newWaveformId);
}

float Looper::processSample (float input)
{
    float output = 0.f;
    if (playState.get() == true)
    {
        //play
        output = *audioSampleBuffer.getWritePointer (0, metronome.getBufferPosition());
        
        //Using a tempOsc so it doesn't go forward by two samples each time this function is called
        float tempOsc = voivces[0]->nextSample() + voivces[1]->nextSample() + voivces[2]->nextSample();
        output += tempOsc;
        
        //click 5 times each bufferLength
        if (metronome.getMetronome() == true)
        {
            metronomeClick = metronome.getMetronome();
            output += 0.25f;
        }
        
        //record
        if (recordState.get() == true)
        {
            audioSample = audioSampleBuffer.getWritePointer(0, metronome.getBufferPosition());
            if (inputState.get() == false)
            {
                *audioSample = input;
            }
            else
            {
                *audioSample = tempOsc;
            }
        }
    }
    return output * amplitude;
}

void Looper::deleteLoop()
{
    for (int counter = 0; counter < metronome.getBufferSize(); counter++)
    {
        audioSample = audioSampleBuffer.getWritePointer(0, counter);
        *audioSample = 0.f;
    }
}

void Looper::setFrequency(int newFrequency)
{
    if (newFrequency > 0.f) {
        //If any of the voices are unused, the new frequency uses that unused voice
        for (int counter = 0; counter < 3; counter++) {
            if (voices[counter]->getFrequency == 0.f) {
                voices[counter]->setFrequency = newFrequency;
            }
        }
        
        //If all voices are in use, it uses the oldest voice
        if (voiceTracker == 0) {
            voices[0]->setFrequency(newFrequency);
            voiceTracker = 1;
        }
        else if (voiceTracker == 1) {
            voices[1]->setFrequency(newFrequency);
            voiceTracker = 2;
        }
        else if (voiceTracker == 2) {
            voices[2]->setFrequency(newFrequency);
            voiceTracker = 0;
        }
    }
    


    osc->setFrequency(newFrequency);
}

void Looper::setAmplitude(float newAmplitude)
{
    osc->setAmplitude(newAmplitude);
}

void Looper::setOverallGain(float newGain)
{
    amplitude = newGain;
}

void Looper::setSampleRate(int newSampleRate)
{
    oscSin.setSampleRate (newSampleRate);
    oscSquare.setSampleRate (newSampleRate);
    oscSaw.setSampleRate (newSampleRate);
    attackRelease.setSampleRate(newSampleRate);
    metronome.setSampleRate(newSampleRate);
}

bool Looper::getMetronomeClick()
{
    return metronomeClick;
}