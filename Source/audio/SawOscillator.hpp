//
//  SawOscillator.hpp
//  JuceBasicAudio
//
//  Created by Kyle Edwards on 01/12/2016.
//
//

#ifndef SawOscillator_hpp
#define SawOscillator_hpp

#include <stdio.h>
#include "Oscillator.hpp"

/**
 Class for a sawtooth wave oscillator.
 @see Oscillator class for base class info
 @see Oscillator
 */

class SawtoothOscillator  : public Oscillator

{
public:
    /**Oscillator constructor*/
    SawtoothOscillator();
    
    /**Oscillator destructor*/
    virtual ~SawtoothOscillator();
    
    /** function that provides the execution of the waveshape
     @param float currentPhase is the phase position of the waveform and it's usually between 0 and 2*M_PI
     @return returns a float amplitude value of the waveform at the current phase position. As this is a sawtooth wave, it will be between -1 and +1 */
    float renderWaveShape(const float currentPhase) override;
    
private:
    
    
};

#endif /* SawOscillator_hpp */
