/*
  ==============================================================================

    Audio.h
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#ifndef AUDIO_H_INCLUDED
#define AUDIO_H_INCLUDED

/**
 Class containing all audio processes
 */

#include "../../JuceLibraryCode/JuceHeader.h"
#include "Looper.h"
#include "Metronome.hpp"
#include "FilePlayer.hpp"

class Audio :   public MidiInputCallback,
                public AudioIODeviceCallback
{
public:
    /** Constructor */
    Audio();
    
    /** Destructor */
    ~Audio();
    
    /** Returns the audio device manager, don't keep a copy of it! */
    AudioDeviceManager& getAudioDeviceManager() { return audioDeviceManager; }
    
    /** Creates the connection from looper to MainComponent */
    Looper& getLooper1() { return *loopers[0]; }
    Looper& getLooper2() { return *loopers[1]; }
    
    /** Creates the connection from metronome to MainComponent */
    Metronome& getMetronome() { return metronome; }

    void handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message) override;
    void audioDeviceIOCallback (const float** inputChannelData,
                                int numInputChannels,
                                float** outputChannelData,
                                int numOutputChannels,
                                int numSamples) override;
    
    void audioDeviceAboutToStart (AudioIODevice* device) override;
    void audioDeviceStopped() override;
    void setPlayState(bool newPlayState);
    void setInputState(bool newInputState);
    bool getInputState();
    
private:
    AudioDeviceManager audioDeviceManager;
    AudioSourcePlayer audioSourcePlayer;
    
    OwnedArray<Looper> loopers;
    Metronome metronome;
    FilePlayer metronomeFilePlayer;
    bool playState;
    bool inputState;
    
    //Timer metronomeTimer;
};



#endif  // AUDIO_H_INCLUDED
